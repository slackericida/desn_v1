function [Hq1] = analisi_MDFA()

scmin=16;
scmax=1024;
scres=19;
exponents=linspace(log2(scmin),log2(scmax),scres);
scale=round(2.^exponents);
q=linspace(-5,5,101);
m=1;
[Hq1,tq1,hq1,Dq1,Fq1]=MFDFA1(est_noise,scale,q,m,1);

end