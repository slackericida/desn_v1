function NRMSE = rmse(estimatedOutput, correctOutput)
% Root Mean Squared Error

NRMSE = sqrt(sum((estimatedOutput - correctOutput).^2)*1/length(estimatedOutput));


end

