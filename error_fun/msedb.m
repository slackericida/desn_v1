% MSEDB - Compute the mean-squared error in DB.
%   After computing the MSE err, this simply performs the transformation
%   err = 10log10(err)

function MSEDB = msedb(estimatedOutput, correctOutput)

meanerror = sum((estimatedOutput - correctOutput).^2)/size(estimatedOutput, 1) ; 
MSEDB = 10*log10(meanerror) ; 
