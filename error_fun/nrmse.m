function NRMSE = nrmse(estimatedOutput, correctOutput)
% Computes the normalized MSE between estimated and correct ESN outputs.
% 
% INPUT:
% - estimatedOutput: array of size N1 x outputDimension, containing network
%   output data. Caution: it is assumed that these are un-rescaled and
%   un-shifted, that is, the transformations from the original data format
%   via esn.teacherScaling and esn.teacherShift are undone. This happens
%   automatically when the estimatedOutput was obtained from calling
%   test_esn.
%
% - correctOutput: array of size N2 x outputDimension, containing the
%   original teacher data. 
%
% OUTPUT:
% - err: a row vector of NRMSE's, each corresponding to one of the output
%   dimensions.

  
nEstimatePoints = size(estimatedOutput, 1) ; 

nForgetPoints = size(correctOutput, 1) - nEstimatePoints ; 

correctOutput = correctOutput(nForgetPoints+1:end,:) ; 

correctVariance = var(correctOutput) ; 
meanerror = sum((estimatedOutput - correctOutput).^2)/nEstimatePoints ; 

NRMSE = (sqrt(meanerror./correctVariance)) ; 
