function [ MRE ] = mre(estimatedOutput, correctOutput)
%MRE Mean Relative Error

MRE = 100/length(estimatedOutput)*sum(abs(estimatedOutput - correctOutput)./correctOutput);
end

