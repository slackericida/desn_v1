% CONFIG_SIMULATION - Define the configuration for the current run

function [config, obsData_tr, obsData_ts, futureData_tr, futureData_ts] = config_simulation(X, forecastStep, signal_id)

% -------------------------------------------------------------------------
% --- Dataset -------------------------------------------------------------
% -------------------------------------------------------------------------  
% [ series ] = dataNorm( series, 6 ); % type of data-normalization
config.forecastStep = forecastStep; % how far the prediction is made
config.dropout = 100; % Number of dropout elements of the ESN
config.test_periods = 50000; % number of time intervals to be predicted
config.K = 1; 
X = [X, ones(length(X),1)];

% construct input and output
[obsData, futureData] = construct_output(X, config.forecastStep);

% Construct Training and Test set
tr_size = (floor(size(obsData,1)) - config.dropout - config.test_periods);
obsData_tr = obsData(1:tr_size, :);
futureData_tr = futureData(1:tr_size, 1);
obsData_ts = obsData(tr_size+1:end, :);
futureData_ts = futureData(tr_size+1:end, 1);

% -------------------------------------------------------------------------
% --- Error measure -------------------------------------------------------
% -------------------------------------------------------------------------
%config.err_fcn = @mre;     % Mean Relative Error
config.err_fcn = @nrmse;    % Normalized Root Mean-Squared Error

% -------------------------------------------------------------------------
% --- ESN parameters ------------------------------------------------------
% -------------------------------------------------------------------------
config.rseed = rng;                                                % seed used for generating the network

% Fixed params
config.n_runs = 1;                                                 % Repetitions of the simulation
config.input_shift = 0;                                            % Input shift
config.teacher_shift = 0;                                          % Teacher shift
config.connectivity = 0.25;                                        % Percentage of non-zero connections in the reservoir                                          
config.rTrain = 'lin_reg';                                         % Readout Training
config.noise_level = 0;                                      % Variance of noise during state update
config.input_scaling = 0.1;                                  % Input scaling
config.feedback_scaling = 0;                                 % Feedback scaling (set to 0 for no feedback)
config.teacher_scaling = 0.1;                                % Teacher scaling

if(strcmp(signal_id,'Y1'))
    config.reservoir_size = 500; 
    config.desired_specrad = 0.99;    
    config.lambda = 0.1;                                           
elseif(strcmp(signal_id,'Y2'))
    config.reservoir_size = 200; 
    config.desired_specrad = 0.4;    
    config.lambda = 0.1;  
elseif(strcmp(signal_id,'Y3'))
    config.reservoir_size = 500; 
    config.desired_specrad = 0.99;    
    config.lambda = 0.1; 
elseif(strcmp(signal_id,'Y4'))
    config.reservoir_size = 400; 
    config.desired_specrad = 0.99;    
    config.lambda = 0.1; 
elseif(strcmp(signal_id,'Y5'))
    config.reservoir_size = 100; 
    config.desired_specrad = 0.99;    
    config.lambda = 0.05; 
elseif(strcmp(signal_id,'Y6'))
    config.reservoir_size = 500; 
    config.desired_specrad = 0.99;    
    config.lambda = 0.1; 
elseif(strcmp(signal_id,'Y7'))
    config.reservoir_size = 500; 
    config.desired_specrad = 0.99;    
    config.lambda = 0.05; 
elseif(strcmp(signal_id,'sunspot'))
    config.reservoir_size = 500; 
    config.desired_specrad = 0.99;    
    config.lambda = 0.05; 
end

end