function [ error, trainingInfo ] = train_esn(trainInputSequence, trainOutputSequence, testInputSequence, testOutputSequence, esn, config )
% TRAIN_ESN - Train the centralized ESN (C-ESN)
% Inputs:
%   - trainInputSequence: train input sequences.
%   - trainOutputSequence: train output sequences.
%   - testInputSequence: test input sequences.
%   - testOutputSequence: test output sequence.
%   - esn: ESN struct created with generate_esn.m.
%   - config: struct with configuration parameters created in config.m.
% Outputs:
%   - error: error of C-ESN.
%   - training_time: training time of C-ESN.
%   - trainingInfo: struct with training information, i.e. the state and
%   teacher matrices, the time required to compute them, the final ESN,
%   etc.

dropout = config.dropout;
tic;

% Compute state and teacher matrices
stateCollection = gather_states(trainInputSequence, trainOutputSequence, esn, dropout, config);
teacherCollection = gather_teacher(trainOutputSequence, esn, dropout, true);

% Compute the output matrix/svr model
if(strcmp(config.rTrain, 'lin_svr'))
    esn.svrModel = svmtrain(teacherCollection, stateCollection, config.lin_svrParams);
elseif(strcmp(config.rTrain, 'rbf_svr'))
    esn.svrModel = svmtrain(teacherCollection, stateCollection, config.rbf_svrParams);
elseif(strcmp(config.rTrain, 'lasso'))
    esn.outputWeights = lasso(stateCollection, teacherCollection, 'Lambda', config.lambda);
    esn.outputWeights = esn.outputWeights';
else % linear regression
    esn.outputWeights = ((stateCollection'*stateCollection + config.lambda*eye(esn.nInputUnits + esn.nInternalUnits))\stateCollection'*teacherCollection)';
end

trainingInfo.gathering_time = toc;
trainingInfo.esn = esn;

% Test the ESN
error = test_esn(testInputSequence, testOutputSequence, esn, config);
    

end

