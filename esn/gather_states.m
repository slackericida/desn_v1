function stateCollection = gather_states( inputSequences, outputSequences, esn, nForgetPoints, config )
% GATHER_STATES - Compute the state matrix.
% Inputs:
%   - inputSequences: input sequences.
%   - outputSequences: output sequences.
%   - esn: ESN struct created with generate_esn.m.
%   - nForgetPoints: number of dropout elements.
% Outputs:
%   - stateCollection: state matrix.

sampleSize = 0;
nTimeSeries = size(inputSequences, 1);
for i = 1:nTimeSeries
    sampleSize = sampleSize + size(inputSequences{i,1},1) - max([0, nForgetPoints]);
end

% collect input+reservoir states into stateCollection
stateCollection = zeros(sampleSize, esn.nInputUnits + esn.nInternalUnits);
collectIndex = 1;
for i = 1:nTimeSeries
    if(isempty(outputSequences))
        stateCollection_i = compute_statematrix(inputSequences{i,1}, [], esn, nForgetPoints, config);
    else
        stateCollection_i = compute_statematrix(inputSequences{i,1}, outputSequences{i,1}, esn, nForgetPoints, config);
    end
    l = size(stateCollection_i, 1);
    stateCollection(collectIndex:collectIndex+l-1, :) = stateCollection_i;
    collectIndex = collectIndex + l;
end

end

