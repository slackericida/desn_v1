function teachCollectMat = compute_teacher(outputSequence, esn, nForgetPoints, shift)
% Copyright: Fraunhofer IAIS 2006 / Patent pending

if(nargin < 4)
    shift = true;
end

% delete the first nForgetPoints elements from outputSequence
if nForgetPoints >= 0
    outputSequence = outputSequence(nForgetPoints+1:end,:) ; 
end

% update the size of outputSequence
nOutputPoints  = length(outputSequence(:,1)) ; 

if(shift)
    teachCollectMat = [(diag(esn.teacherScaling) * outputSequence')' + repmat(esn.teacherShift',[nOutputPoints 1])];
else
    teachCollectMat = outputSequence;
end




