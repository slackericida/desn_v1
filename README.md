# README #

Code implementation of the methods presented in "Data-driven detrending of nonstationary fractal time series with echo state networks".
A preprint of the paper can be found on [Arxiv](http://arxiv.org/abs/1510.07146).

### Content ###

**Algorithms:**

The repository contains 4 different data-driven methods for removing the underlying trend from fractal and multifractal time series. 

* DESN (Detrending with Echo State Network);
* EMD (Empirical Mode Decomposition);
* FDFA (Fourier Detrend Fluctuation Analysis);
* SM (SMoothing detrending).

**Dataset**

* 5 synthetic signals;
* 3 different types of (multi) fractal noise;
* Sunspot data

### How do I get set up? ###

Each algorithm can be run by executing the relative script file, namely testESN.m, testEMD, testSM, testFDFA.
To select one of the 8 tasks presented in the paper, modify the string assigned to the variable *signal_id* at the beginning (line 8) of each file. Es:

```
#!matlab
% specify which test to do
signal_id = 'sunspot';
```

Possible values for signal_id are: 'Y1', 'Y2', ..., 'Y7', 'sunspot'.

To plot the results and other graphics useful for analysis, set the variable *plot_on=1* at the beginning of the file.

### Who do I talk to? ###
Repo owner/admins:

* Filippo M. Bianchi (filippo.m.bianchi@uit.no)
* Enrico Maiorino (enrico.maiorino@gmail.com)