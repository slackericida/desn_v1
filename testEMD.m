close all
clear

folds = addpath(genpath(pwd));
plot_on=1; save_on=0;

% specify which test to do
signal_id = 'sunspot';

if(strcmp(signal_id,'Y1'))
    load('s1.mat')
    load('brownNoise0.7.mat')
    alpha = 0.5;
elseif(strcmp(signal_id,'Y2'))
    load('s2.mat')
    load('brownNoise0.7.mat')
    alpha = 0.5;
elseif(strcmp(signal_id,'Y3'))
    load('s3.mat')
    load('brownNoise0.7.mat')
    alpha = 0.1;
elseif(strcmp(signal_id,'Y4'))
    load('s4.mat')
    load('brownNoise0.7.mat')
    alpha = 0.1;
elseif(strcmp(signal_id,'Y5'))
    load('s5.mat')
    load('brownNoise0.7.mat')
    alpha = 0.5;
elseif(strcmp(signal_id,'Y6'))
    load('s1.mat')
    load('brownNoise0.3.mat')
    alpha = 0.1;
elseif(strcmp(signal_id,'Y7'))
    load('s7.mat')
    alpha = 0.1;
elseif(strcmp(signal_id,'sunspot'))
    load('sunspot_D.mat')
    alpha = 0.5;
end

% init data structures
[Theta, Mfwidth, Hurst] = deal(zeros(size(noise,2),1));

% main cycle
for j=1:size(noise,2)

    disp(['It: ',num2str(j)])
    
    if(~strcmp(signal_id,'sunspot'))
        noise_i = noise(:,j);
        series = signal + noise_i;
        m=2;
    else
        m=1;
    end
    
    test_series = series;
    test_signal = signal;
    test_noise = noise;

    IMF = emd(test_series);
    nIMF = size(IMF,1);

    est_signal = 0;
    costMax = inf;
    ind = -1;
    C = [];
    
    for k=1:nIMF
        est_signal = est_signal + IMF(end-k+1,:)';
        err = nrmse(est_signal, test_series);
        cost = alpha*err + (1-alpha)*k/nIMF;
        C = [C;cost];
        if(cost < costMax)
            costMax = cost;
            ind = k;
        end
    end
    
    est_signal = 0;
    for k=1:ind
        est_signal = est_signal + IMF(end-k+1,:)';
    end
    est_noise = test_series - est_signal;
    
    if(plot_on)
        figure
        plot(test_series, 'k--','LineWidth',1)
        hold on
        plot(test_signal, 'r-','LineWidth',1)
        plot(est_signal)
        title('trend')
        hold off
        legend({'trend+noise','trend (true)','trend (estimated)'})
    end
    
    if(plot_on)
        figure
        plot(test_noise(:,j), 'k--','LineWidth',1)
        hold on
        plot(est_noise)
        title('noise')
        hold off
        legend({'noise (true)','noise (estimated)'})
    end

    if(plot_on)
        figure
        plot(C)
        xlabel('# IMF')
        ylabel('Cost function')
    end

    if(save_on)
        formatOut = 'mmdd-HHMM';
        save(['EMD',num2str(j),'_',datestr(now,formatOut),'.mat'],'est_noise','series','noise','signal')
    end
    
    if(plot_on)
        disp(['Using the last ',num2str(ind),' IMFs'])
    end
    
    % MFDFA
    scmin=16;
    scmax=1024;
    scres=19;
    exponents=linspace(log2(scmin),log2(scmax),scres);
    scale=round(2.^exponents);
    q=linspace(-5,5,101);
    [Hq1,~,hq1,~,~]=MFDFA1(est_noise,scale,q,m,plot_on);
    [hurst_j,mfwidth_j,mfwl,mfwr] = get_hurst_mfwidth(q,Hq1,hq1);
    theta_j = (mfwl - mfwr)/(mfwl + mfwr);

    [Theta(j), Mfwidth(j), Hurst(j)] = deal(theta_j, mfwidth_j, hurst_j);

end

disp(['Hurst: ',num2str(mean(Hurst)), ' (',num2str(std(Hurst)), ')']);
disp(['MFW: ',num2str(mean(Mfwidth)), ' (',num2str(std(Mfwidth)), ')']);
disp(['Theta: ',num2str(mean(Theta)), ' (',num2str(std(Theta)), ')']);

rmpath(genpath(fileparts(pwd)));