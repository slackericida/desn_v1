function [hurst, mfwidth, mfwl, mfwr] = get_hurst_mfwidth(q, Hq, hq)
    hurst = Hq(q==2);
    mfwidth = max(hq) - min(hq);
    mfwl = abs(hq(floor(length(q)/2)) - min(hq));
    mfwr = abs(hq(floor(length(q)/2)) - max(hq));
end