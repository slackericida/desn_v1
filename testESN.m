close all; 
clear

folds = addpath(genpath(fileparts(pwd)));
plot_on=1; save_on=0;

% specify which test to do
signal_id = 'Y7';

if(strcmp(signal_id,'Y1'))
    load('s1.mat')
    load('brownNoise0.7.mat')
    n_runs = 30;
elseif(strcmp(signal_id,'Y2'))
    load('s2.mat')
    load('brownNoise0.7.mat')
    n_runs = 20;
elseif(strcmp(signal_id,'Y3'))
    load('s3.mat')
    load('brownNoise0.7.mat')
    n_runs = 20;
elseif(strcmp(signal_id,'Y4'))
    load('s4.mat')
    load('brownNoise0.7.mat')
    n_runs = 10;
elseif(strcmp(signal_id,'Y5'))
    load('s5.mat')
    load('brownNoise0.7.mat')
    n_runs = 30;
elseif(strcmp(signal_id,'Y6'))
    load('s1.mat')
    load('brownNoise0.3.mat')
    n_runs = 30;
elseif(strcmp(signal_id,'Y7'))
    load('s7.mat')
    n_runs = 20;
elseif(strcmp(signal_id,'sunspot'))
    load('sunspot_D.mat')
    n_runs = 10;
    %n_runs = 30;
end

% init data structures
[Theta, Mfwidth, Hurst] = deal(zeros(size(noise,2),1));

% main cycle
for j=1:size(noise,2);

    if(~strcmp(signal_id,'sunspot'))
        noise_i = noise(:,j);
        series = signal + noise_i;
        m=2;
    else
        m=1;
    end

    esn_forecast = 0;

    for i=1:n_runs

        forecastStep = i*10;

        % Load configuration
        [config, obsData_tr, obsData_ts, futureData_tr, futureData_ts] = config_simulation(series, forecastStep, signal_id);

        % ESN
        rng(config.rseed)
        [ esn_forecast_i, ~, ~, ~, ~ ] = esn_prediction( {obsData_tr}, {obsData_ts}, {futureData_tr}, {futureData_ts}, config );

        % create ensemble
        esn_forecast = esn_forecast + esn_forecast_i;

    end
    
    X_test = series(end-config.test_periods+1:end);
    esn_forecast = esn_forecast / n_runs;
    esn_error = config.err_fcn(esn_forecast, X_test);
    est_noise = X_test - esn_forecast;
    noise_test = noise(end-config.test_periods+1:end,j);
    signal_test = signal(end-config.test_periods+1:end);
    est_corr = corr(noise_test,est_noise);
    noise_err = config.err_fcn(noise_test, est_noise);

    % Plot prediction results
    if(plot_on)
        plot_periods = config.test_periods;
        legendNames = cell(0);
        legendNames{1} = 'Observed';
        legendNames{2} = 'ESN';
        plot_outputs('Prediction Results', legendNames, X_test(1:plot_periods), [esn_forecast(1:plot_periods), signal_test]);
        figshift;
    end

    % plot noise estimation
    if(plot_on)
        legendNames = cell(0);
        legendNames{1} = 'True Noise';
        legendNames{2} = 'Est. Noise';
        plot_outputs('Noise Estimation', legendNames, noise_test(1:plot_periods), [est_noise(1:plot_periods)]);
        figshift;
    end
    
    % plot autocorr
    if(plot_on)
        figure
        plot(autocorr(est_noise, 500))
        hold on
        plot(autocorr(noise_test, 500))
    end

    % MFDFA
    scmin=16;
    scmax=1024;
    scres=19;
    exponents=linspace(log2(scmin),log2(scmax),scres);
    scale=round(2.^exponents);
    q=linspace(-5,5,101);
    [Hq1,~,hq1,~,~]=MFDFA1(est_noise,scale,q,m,1);
    [hurst_j,mfwidth_j,mfwl,mfwr] = get_hurst_mfwidth(q,Hq1,hq1);
    theta_j = (mfwl - mfwr)/(mfwl + mfwr);
    
    % Display results
    res = [esn_error; est_corr; noise_err; config.forecastStep; config.test_periods; config.reservoir_size; config.desired_specrad;...
        config.noise_level; config.lambda; config.input_scaling;...
        config.feedback_scaling; config.teacher_scaling; hurst_j; mfwidth_j; theta_j];
    disptable(res, {'Parameters'}, {'pred. err', 'corr', 'noise err', 'forecast step','test period','reservoir size','spec rad',...
        'noise level', 'lambda', 'input scale', 'feedback scale', 'teacher scale', 'Hurst', 'MFW', 'Theta'});

    % write the main results of the simulation on file
    if(save_on)
        formatOut = 'mmdd-HHMM';
        save(['ESN_',datestr(now,formatOut),'.mat'],'configSet','est_noise','esn_forecast','series','noise','signal','Hurst')
    end
    
    % save results
    [Theta(j), Mfwidth(j), Hurst(j)] = deal(theta_j, mfwidth_j, hurst_j);

end

% display results on the screen
disp(['Hurst: ',num2str(mean(Hurst)), ' (',num2str(std(Hurst)), ')']);
disp(['MFW: ',num2str(mean(Mfwidth)), ' (',num2str(std(Mfwidth)), ')']);
disp(['Theta: ',num2str(mean(Theta)), ' (',num2str(std(Theta)), ')']);

rmpath(genpath(fileparts(pwd)));