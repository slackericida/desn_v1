% SINGLECOLUMN_FORMAT - Resize and reposition the current figure
% This is handy for article publishing.

set(gca, 'FontSize', font_size);
set(gca, 'FontName', font_name);

if(exist('h_legend', 'var'))
    set(h_legend,'FontSize', font_size_leg);
    set(h_legend,'FontName', font_name);
end

% Set the default Size for display
set(gcf, 'PaperUnits', 'inches');
defpos = get(gcf, 'Position');
set(gcf,'Position', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(gcf, 'PaperPosition', defsize);