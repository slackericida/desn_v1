% PLOT_OUTPUTS 
function [] = plot_outputs(title, legendNames, realValues, predictValues)

% Fig properties
width = 3.45;                   % Width in inches
height = 2.6;                   % Height in inches
font_size = 8;                  % Fontsize
font_size_leg = 6;              % Font size (legend)
font_name = 'TimesNewRoman';    % Font name
line_width = 1;                 % LineWidth

figure('name', title);

% Number of points to be plotted
n_points = length(realValues);

plot(1:n_points, realValues, 'k--','LineWidth', line_width);
hold on;
plot(1:n_points, predictValues,'LineWidth', line_width);

box on;
grid on;

xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
ylabel('Value', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend(legendNames, 'Location', 'NorthWest');

%%%%%%%%%%%%% singlecolumn_format %%%%%%%%%%%%%%
set(gca, 'FontSize', font_size);
set(gca, 'FontName', font_name);

if(exist('h_legend', 'var'))
    set(h_legend,'FontSize', font_size_leg);
    set(h_legend,'FontName', font_name);
end

% Set the default Size for display
set(gcf, 'PaperUnits', 'inches');
defpos = get(gcf, 'Position');
set(gcf,'Position', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(gcf, 'PaperPosition', defsize);

end