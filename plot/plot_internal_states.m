% PLOT_INTERNAL_STATES - Plot a selection of the internal states of C-ESN

figure('name', 'C-ESN States');
hold on

x = 1:100; % Number of randomly selected neurons in the reservoir
neurons_idx = randsample(config.reservoir_size, 20);

plot(x, stateMatrix(x, neurons_idx), 'LineWidth', line_width);
xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
ylabel('Value', 'FontSize', font_size, 'FontName', font_name);
ylim([min(min(stateMatrix(x, neurons_idx))), max(max(stateMatrix(x, neurons_idx)))]);
singlecolumn_format;