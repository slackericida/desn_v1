% MAKE_PLOTS - Plot information for the current simulation

n_points = 100;

% General properties for the figures
fig_prop.width = 3.45;                   % Width in inches
fig_prop.height = 2.6;                   % Height in inches
fig_prop.font_size = 8;                  % Fontsize
fig_prop.font_size_leg = 6;              % Font size (legend)
fig_prop.font_name = 'TimesNewRoman';    % Font name
fig_prop.line_width = 1;                 % LineWidth
legendNames = cell(0);
legendNames{1} = 'Real';
legendNames{2} = 'Predicted';
legendNames{3} = 'Random';

% Plot outputs
plot_outputs('C-ESN outputs',legendNames,teacherCollection(1:n_points), [predictedTestOutput(1:n_points), rand(100,1)], fig_prop);
figshift;

% % Plot internal states
% plot_internal_states;
% figshift;