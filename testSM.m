close all
clear

folds = addpath(genpath(pwd));
plot_on=1; save_on=0; degree = 15;

% specify which test to do
signal_id = 'Y1';

if(strcmp(signal_id,'Y1'))
    load('s1.mat')
    load('brownNoise0.7.mat')
    span = 50;
elseif(strcmp(signal_id,'Y2'))
    load('s2.mat')
    load('brownNoise0.7.mat')
    span = 1800;
elseif(strcmp(signal_id,'Y3'))
    load('s3.mat')
    load('brownNoise0.7.mat')
    span = 20;
elseif(strcmp(signal_id,'Y4'))
    load('s4.mat')
    load('brownNoise0.7.mat')
    span = 10;
elseif(strcmp(signal_id,'Y5'))
    load('s5.mat')
    load('brownNoise0.7.mat')
    span = 1000;
elseif(strcmp(signal_id,'Y6'))
    load('s1.mat')
    load('brownNoise0.3.mat')
    span = 50;
elseif(strcmp(signal_id,'Y7'))
    load('s7.mat')
    span = 60;
elseif(strcmp(signal_id,'sunspot'))
    load('sunspot_D.mat')
    span = 1000;
end

% init data structures
[Theta, Mfwidth, Hurst] = deal(zeros(size(noise,2),1));

% main cycle
for j=1:size(noise,2);
    
    disp(['It: ',num2str(j)])
    if(~strcmp(signal_id,'sunspot'))
        noise = noise(:,j);
        series = signal + noise;
        m=2;
    else
        m=1;
    end
  
    if(strcmp(signal_id,'Y1'))
        Ys = smooth(series,span, 'loess');
    elseif(strcmp(signal_id,'Y2'))
        Ys = smooth(series,span, 'lowess');
    elseif(strcmp(signal_id,'Y3'))
        Ys = smooth(series,span, 'sgolay', degree);
    elseif(strcmp(signal_id,'Y4'))
        Ys = smooth(series,span);
    elseif(strcmp(signal_id,'Y5'))
        Ys = smooth(series,span);
    elseif(strcmp(signal_id,'Y6'))
        Ys = smooth(series,span, 'loess');
    elseif(strcmp(signal_id,'Y7'))
        Ys = smooth(series,span, 'sgolay', degree);
    elseif(strcmp(signal_id,'sunspot'))
        Ys = smooth(series,span, 'loess');
    end
    err = nrmse(Ys, series);

    if(plot_on)
        hold on
        plot(series, 'k--','LineWidth',1)
        plot(signal, 'r-','LineWidth',1)
        plot(Ys)
        hold off
        legend({'trend+noise','trend (true)','trend (estimated)'})
        title('trend')
    end

    est_noise = series - Ys;
    if(plot_on)
        figure
        hold on
        plot(noise, 'k--','LineWidth',1.5);
        plot(est_noise);
        hold off
        legend('noise (true)','noise (estimated)')
        title('noise')
    end

    sm_corr = corr(est_noise, noise);
    if(plot_on)
        disp(['NRMSE(Y,Ys) = ',num2str(err),' -- corr(noise,est_noise) = ',num2str(sm_corr)])
    end
    
    if(save_on)
        formatOut = 'mmdd-HHMM';
        save(['SM',num2str(j),'_',datestr(now,formatOut),'.mat'],'span','degree','est_noise','series','noise','signal')
    end
    
    % MFDFA
    scmin=16;
    scmax=1024;
    scres=19;
    exponents=linspace(log2(scmin),log2(scmax),scres);
    scale=round(2.^exponents);
    q=linspace(-5,5,101);
    [Hq1,tq1,hq1,Dq1,Fq1]=MFDFA1(est_noise,scale,q,m,1);
    [hurst_j,mfwidth_j,mfwl,mfwr] = get_hurst_mfwidth(q,Hq1,hq1);
    theta_j = (mfwl - mfwr)/(mfwl + mfwr);

    [Theta(j), Mfwidth(j), Hurst(j)] = deal(theta_j, mfwidth_j, hurst_j);
    
end

disp(['Hurst: ',num2str(mean(Hurst)), ' (',num2str(std(Hurst)), ')']);
disp(['MFW: ',num2str(mean(Mfwidth)), ' (',num2str(std(Mfwidth)), ')']);
disp(['Theta: ',num2str(mean(Theta)), ' (',num2str(std(Theta)), ')']);


rmpath(genpath(fileparts(pwd)));