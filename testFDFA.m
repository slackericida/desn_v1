close all
clear

folds = addpath(genpath(pwd));
plot_on=1; save_on=0;

% specify which test to do
signal_id = 'sunspot';

if(strcmp(signal_id,'Y1'))
    load('s1.mat')
    load('brownNoise0.7.mat')
    truncfreq = 150;
    trunctime = 950;
elseif(strcmp(signal_id,'Y2'))
    load('s2.mat')
    load('brownNoise0.7.mat')
    truncfreq = 60;
    trunctime = 1;
elseif(strcmp(signal_id,'Y3'))
    load('s3.mat')
    load('brownNoise0.7.mat')
    truncfreq = 115;
    trunctime = 50;
elseif(strcmp(signal_id,'Y4'))
    load('s4.mat')
    load('brownNoise0.7.mat')
    truncfreq = 400;
    trunctime = 3000;
elseif(strcmp(signal_id,'Y5'))
    load('s5.mat')
    load('brownNoise0.7.mat')
    truncfreq = 4000;
    trunctime = 250;
elseif(strcmp(signal_id,'Y6'))
    load('s1.mat')
    load('brownNoise0.3.mat')
    truncfreq = 400;
    trunctime = 2000;
elseif(strcmp(signal_id,'Y7'))
    load('s7.mat')
    truncfreq = 250;
    trunctime = 2000;
elseif(strcmp(signal_id,'sunspot'))
    load('sunspot_D.mat')
    truncfreq = 150;
    trunctime = 500;
end

% init data structures
[Theta, Mfwidth, Hurst] = deal(zeros(size(noise,2),1));

% main cycle
for j=1:size(noise,2);

    disp(['It: ',num2str(j)])
    
    if(~strcmp(signal_id,'sunspot'))
        series = signal + noise(:,j);
        m=2;
    else
        m=1;
    end
    
    % first part
    trans = fft(series);
    [~,inds] = sort(abs(trans),'descend');
    mvalues = [];
    for i = 1:10000
        mvalues = [mvalues, max(abs(trans(inds(i:end))))];
    end
    
    if(plot_on)
        figure
        plot(mvalues)
        title('check truncfreq value')
    end
    
    % second part
    trans(inds(1:truncfreq)) = 0; %datasample(trans(inds(trunc - (-1):end)), trunc);
    est_noise = real(ifft(trans));
    
    if(plot_on)
        figure
        plot(est_noise)
        title('check trunctime value')
    end

    % third part
    est_noise = est_noise(trunctime:end-trunctime);
    
    if(plot_on)
        figure
        plot(est_noise);
        title('check if border effects are removed')
    end
       
    if(plot_on)
        figure
        plot(noise(trunctime:end-trunctime+1,j), 'k--','LineWidth',1)
        hold on
        plot(est_noise)
        title('noise')
        hold off
        legend({'noise (true)','noise (estimated)'})
    end
    
    if(save_on)
        formatOut = 'mmdd-HHMM';
        save(['FD',num2str(j),'_',datestr(now,formatOut),'.mat'],'trunctime','truncfreq','est_noise','series','noise','signal')
    end
    
    % MFDFA
    scmin=16;
    scmax=1024;
    scres=19;
    exponents=linspace(log2(scmin),log2(scmax),scres);
    scale=round(2.^exponents);
    q=linspace(-5,5,101);
    [Hq1,~,hq1,~,~]=MFDFA1(est_noise,scale,q,m,plot_on);
    [hurst_j,mfwidth_j,mfwl,mfwr] = get_hurst_mfwidth(q,Hq1,hq1);
    theta_j = (mfwl - mfwr)/(mfwl + mfwr);

    [Theta(j), Mfwidth(j), Hurst(j)] = deal(theta_j, mfwidth_j, hurst_j);

end

disp(['Hurst: ',num2str(mean(Hurst)), ' (',num2str(std(Hurst)), ')']);
disp(['MFW: ',num2str(mean(Mfwidth)), ' (',num2str(std(Mfwidth)), ')']);
disp(['Theta: ',num2str(mean(Theta)), ' (',num2str(std(Theta)), ')']);

rmpath(genpath(fileparts(pwd)));